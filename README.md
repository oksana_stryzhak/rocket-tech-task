## Required software

**The following software is required:**

1. Install Java JDK 11
   
2. Download and install Android Studio (
`set ANDROID_HOME environment variable and add $ANDROID_HOME/platform-tools and $ANDROID_HOME/tools to PATH variable`
3. Install node.js & npm
```  
brew install node
npm install -g npm
```
4. Install Maven
```
brew install maven
```
5. Install Appium
```   
npm install -g appium
npm install wd 
```
6. Prepare and run Android emulator with OS Android 11 / API 30,  Google Play and Google Chrome driver (for example Nexus 5X)

## Download project
```git clone https://oksana_stryzhak@bitbucket.org/oksana_stryzhak/rocket-tech-task.git```

## Run tests
1. Get an emulator name using command 
```adb devices```
2. Run test by the following command
```mvn test -DdeviceName={NAME_OF_EMULATED_DEVICE}```

## Generate Allure report

`mvn allure:serve`

Allure report will be generated from test artefacts at target/allure-results directory and automatically opened in default browser.