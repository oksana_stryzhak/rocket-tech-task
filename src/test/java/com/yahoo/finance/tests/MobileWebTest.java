package com.yahoo.finance.tests;

import com.yahoo.finance.DriverService;
import com.yahoo.finance.pages.QuotePage;
import com.yahoo.finance.pages.YahooFinancePage;
import com.yahoo.finance.utils.DateUtil;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

public class MobileWebTest extends DriverService {

    @Test
    public void compareHistoricalAppleStocksWithCurrent() {
        // given
        final String appleCompanyShortName = "AAPL";
        final String historicalDate = DateUtil.getLastWeekDate();

        // when
        final YahooFinancePage yahooFinancePage = new YahooFinancePage(getDriver());
        yahooFinancePage
                .openMainPage()
                .searchByName(appleCompanyShortName);
        final QuotePage quotePage = new QuotePage(getDriver());
        final String currentStocks = quotePage.getCurrentStocks();
        // and
        final String lastWeekStocks = quotePage.selectHistoricalDataMenu()
                .getOpenStocksByDate(historicalDate);

        // then
        final double currentStocksDouble = Double.parseDouble(currentStocks);
        final double lastWeekStocksDouble = Double.parseDouble(lastWeekStocks);
        final String failReason = String.format("Last week stocks '%s' is less than '%s' current stocks value",
                lastWeekStocksDouble, currentStocksDouble);
        assertThat(failReason, currentStocksDouble, greaterThanOrEqualTo(lastWeekStocksDouble));
    }
}