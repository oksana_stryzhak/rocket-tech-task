package com.yahoo.finance.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtil {

    private static final String MMM_DD_YYYY = "MMM dd, yyyy";

    private static LocalDateTime getNow() {
        return LocalDateTime.now();
    }

    public static String getLastWeekDate() {
        return getNow().minusDays(7).format(DateTimeFormatter.ofPattern(MMM_DD_YYYY));
    }

}
