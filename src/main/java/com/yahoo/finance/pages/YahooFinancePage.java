package com.yahoo.finance.pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class YahooFinancePage {

    private final AppiumDriver<MobileElement> driver;

    public YahooFinancePage(final AppiumDriver<MobileElement> driver) {
        this.driver = driver;
    }

    @Step
    public YahooFinancePage openMainPage() {
        driver.get("https://finance.yahoo.com/");
        return this;
    }

    @Step
    public void searchByName(final String name) {
        final MobileElement searchInputField = driver.findElement(By.name("yfin-usr-qry"));
        searchInputField.sendKeys(name);
        searchInputField.sendKeys(Keys.ENTER);
    }

}