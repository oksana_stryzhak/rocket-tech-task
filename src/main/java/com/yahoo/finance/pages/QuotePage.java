package com.yahoo.finance.pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.openqa.selenium.By;

public class QuotePage {

    private final AppiumDriver<MobileElement> driver;

    public QuotePage(final AppiumDriver<MobileElement> driver) {
        this.driver = driver;
    }

    @Step
    public String getCurrentStocks() {
        return driver.findElements(By.cssSelector("div[id=quote-header-info] div:nth-child(3) span")).get(0).getText();
    }

    @Step
    public QuoteHistoryPage selectHistoricalDataMenu() {
        driver.findElement(By.cssSelector("li[data-test='HISTORICAL_DATA']")).click();
        Assert.assertTrue(driver.findElement(By.cssSelector("table[data-test='historical-prices']")).isDisplayed());
        return new QuoteHistoryPage(driver);
    }
}