package com.yahoo.finance.pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class QuoteHistoryPage {

    private final AppiumDriver<MobileElement> driver;

    public QuoteHistoryPage(final AppiumDriver<MobileElement> driver) {
        this.driver = driver;
    }

    @Step
    public String getOpenStocksByDate(final String date) {
        final MobileElement historyStocksRow = driver.findElements(By.cssSelector("tr"))
                .stream()
                .filter(row -> row.getText().contains(date)).findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Not found row with '%s' date", date)));
        final MobileElement openStocksCell = historyStocksRow.findElements(By.xpath("td")).get(1);
        return openStocksCell.getText();
    }
}